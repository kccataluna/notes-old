 〖Installation〗
 
  ❐ https://github.com/axios/axios
  
  ► npm install axios
├
 〖GET Request〗
 
  ► import Axios from 'axios';
  
    ...
  
  ► Axios({
  ►     method: 'GET',
  ►     url: <URL>
  ► }).then((response) => {
  ►     ...
  ► });